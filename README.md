# Api of project Friendisc

### Technologies used:
<ul>
    <li>FrameWork PHP: Symfony</li>
</ul>

### Installation
```
1 > git clone https://gitlab.com/baptiste.brand/friendisc-api.git
2 > cd friendisc-api
3 > composer install 
4 > Edit .env or create .env.local
5 > php bin/console d:d:c / doctrine:database:create
6 > php bin/console d:s:u --force / doctrine:schema:update --force
8 > symfony serve
```
