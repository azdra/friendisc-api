<?php

namespace App\Entity\Server;

use App\Entity\User\User;
use App\Repository\MemberRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=MemberRepository::class)
 * @ORM\Table(name="member")
 */
class Member
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"messageServer", "sendMessage"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"messageServer", "sendMessage"})
     */
    private $nickname;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"messageServer"})
     */
    private $joint_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User", inversedBy="members")
     * @Groups({"messageServer", "sendMessage"})
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Server\Server", inversedBy="members")
     */
    private $server;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Message\MessageServer", mappedBy="owner")
     */
    private $messageServers;

    public function __construct()
    {
        $this->setJointAt(new \DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getJointAt(): ?\DateTimeInterface
    {
        return $this->joint_at;
    }

    public function setJointAt(\DateTimeInterface $joint_at): self
    {
        $this->joint_at = $joint_at;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getServer(): ?Server
    {
        return $this->server;
    }

    public function setServer(?Server $server): self
    {
        $this->server = $server;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessageServers()
    {
        return $this->messageServers;
    }

    /**
     * @param mixed $messageServers
     */
    public function setMessageServers($messageServers): void
    {
        $this->messageServers = $messageServers;
    }
}
