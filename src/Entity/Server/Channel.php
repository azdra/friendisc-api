<?php

namespace App\Entity\Server;

use App\Entity\Message\MessageServer;
use App\Repository\ChannelRepository;
use Chivincent\Snowflake\Snowflake;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ChannelRepository::class)
 */
class Channel
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=255)
     * @Groups({"serverClick", "loadApp", "createServer", "joinInvite"})
     */
    private string $id;

    /**
     * @ORM\Column(type="string", length=120)
     * @Groups({"serverClick", "loadApp", "createServer", "joinInvite"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"serverClick", "loadApp", "createServer", "joinInvite"})
     */
    private $topic;

    /**
     * @ORM\Column(type="string", length=8)
     * @Groups({"serverClick", "loadApp", "createServer", "joinInvite"})
     */
    private $type;

    /**
     * @ORM\Column(type="datetime")
     */
    private $create_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $update_at;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Message\MessageServer", mappedBy="channel", orphanRemoval=true)
     */
    private $messageServers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Server\Server", inversedBy="channels")
     */
    private $servers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Server\Invite", mappedBy="channel")
     */
    private $invite;

    public function __construct()
    {
        $snowflake = new Snowflake();
        $this->id = $snowflake();
        $this->messageServers = new ArrayCollection();
        $this->servers = new ArrayCollection();
        $this->setCreateAt(new \DateTime());
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->create_at;
    }

    public function setCreateAt(\DateTimeInterface $create_at): self
    {
        $this->create_at = $create_at;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->update_at;
    }

    public function setUpdateAt(?\DateTimeInterface $update_at): self
    {
        $this->update_at = $update_at;

        return $this;
    }

    /**
     * @return Collection|MessageServer[]
     */
    public function getMessageServers(): Collection
    {
        return $this->messageServers;
    }

    public function addMessageServer(MessageServer $messageServer): self
    {
        if (!$this->messageServers->contains($messageServer)) {
            $this->messageServers[] = $messageServer;
            $messageServer->setChannel($this);
        }

        return $this;
    }

    public function removeMessageServer(MessageServer $messageServer): self
    {
        if ($this->messageServers->removeElement($messageServer)) {
            // set the owning side to null (unless already changed)
            if ($messageServer->getChannel() === $this) {
                $messageServer->setChannel(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Server[]
     */
    public function getServers(): Collection
    {
        return $this->servers;
    }

    public function addServer(Server $server): self
    {
        if (!$this->servers->contains($server)) {
            $this->servers[] = $server;
//            $server->addChannel($this);
        }

        return $this;
    }

    public function removeServer(Server $server): self
    {
        if ($this->servers->contains($server)) {
            $this->servers->removeElement($server);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * @param mixed $topic
     */
    public function setTopic($topic): void
    {
        $this->topic = $topic;
    }
}
