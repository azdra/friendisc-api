<?php

namespace App\Entity\Server;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\InviteRepository;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class InviteRepository
 * @package App\Entity\Server
 * @ORM\Entity(repositoryClass=InviteRepository::class)
 */
class Invite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     * @Groups({"joinInvite"})
     */
    private $code;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $uses;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $max_uses;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expire_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User", inversedBy="invite")
     */
    private $owner;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Server\Server", inversedBy="invite")
     * @Groups({"joinInvite"})
     */
    private $server;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Server\Channel", inversedBy="invite")
     */
    private $channel;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->uses = 0;
        $this->setCode(\ShortCode\Random::get());
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getUses()
    {
        return $this->uses;
    }

    /**
     * @param mixed $uses
     */
    public function setUses($uses): void
    {
        $this->uses = $uses;
    }

    /**
     * @return mixed
     */
    public function getMaxUses()
    {
        return $this->max_uses;
    }

    /**
     * @param mixed $max_uses
     */
    public function setMaxUses($max_uses): void
    {
        $this->max_uses = $max_uses;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return mixed
     */
    public function getExpireAt()
    {
        return $this->expire_at;
    }

    /**
     * @param mixed $expire_at
     */
    public function setExpireAt($expire_at): void
    {
        $this->expire_at = $expire_at;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner): void
    {
        $this->owner = $owner;
    }

    /**
     * @return mixed
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * @param mixed $server
     */
    public function setServer($server): void
    {
        $this->server = $server;
    }
}