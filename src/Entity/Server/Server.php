<?php

namespace App\Entity\Server;

use App\Entity\User\User;
use App\Repository\ServerRepository;
use Chivincent\Snowflake\Snowflake;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class Server
 * @package App\Entity\Server
 * @ORM\Entity(repositoryClass=ServerRepository::class)
 * @Vich\Uploadable
 * @UniqueEntity(
 *     fields={"id"},
 *     errorPath="id",
 *     message="There is already an account with this id"
 * )
 */
class Server
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=255)
     * @Groups({"loadApp", "createServer", "joinInvite"})
     */
    private string $id;

    /**
     * @ORM\Column(type="string", length=120)
     * @Groups({"loadApp", "createServer", "joinInvite"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string|null
     * @Groups({"loadApp", "createServer", "joinInvite"})
     */
    private $picture;

    /**
     * @Vich\UploadableField(mapping="server_images", fileNameProperty="picture")
     *
     * @var File|null
     */
    private $picturePath;

    /**
     * @ORM\Column(type="datetime")
     */
    private $create_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $update_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User", inversedBy="own_servers")
     * @Groups({"createServer"})
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Server\Member", mappedBy="server")
     */
    private $members;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Server\Channel", mappedBy="servers")
     * @Groups({"loadApp", "createServer", "joinInvite"})
     */
    private $channels;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Server\Invite", mappedBy="server")
     */
    private $invite;

    public function __construct()
    {
        $snowflake = new Snowflake();
        $this->id = $snowflake();
        $this->members = new ArrayCollection();
        $this->channels = new ArrayCollection();
        $this->setCreateAt(new \DateTime());
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setPicturePath(?File $picturePath = null): void
    {
        $this->picturePath = $picturePath;
        if(null !== $picturePath) {
            $this->update_at = new \DateTime();
        }
    }

    public function getPicturePath(): ?File
    {
        return $this->picturePath;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): void
    {
        $this->picture = $picture;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->create_at;
    }

    public function setCreateAt(\DateTimeInterface $create_at): self
    {
        $this->create_at = $create_at;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->update_at;
    }

    public function setUpdateAt(?\DateTimeInterface $update_at): self
    {
        $this->update_at = $update_at;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection|Member[]
     */
    public function getMembers(): Collection
    {
        return $this->members;
    }

    public function addMember(Member $member): self
    {
        if (!$this->members->contains($member)) {
            $this->members[] = $member;
            $member->setServer($this);
        }

        return $this;
    }

    public function removeMember(Member $member): self
    {
        if ($this->members->removeElement($member)) {
            // set the owning side to null (unless already changed)
            if ($member->getServer() === $this) {
                $member->setServer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Channel[]
     */
    public function getChannels(): Collection
    {
        return $this->channels;
    }

    public function addChannel(Channel $channel): self
    {
        if (!$this->channels->contains($channel)) {
            $this->channels[] = $channel;
            $channel->addServer($this);
        }

        return $this;
    }

    public function removeChannel(Channel $channel): self
    {
        if ($this->channels->contains($channel)) {
            $this->channels->removeElement($channel);
            $channel->removeServer($this);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInvite()
    {
        return $this->invite;
    }

    /**
     * @param mixed $invite
     */
    public function setInvite($invite): void
    {
        $this->invite = $invite;
    }

    /**
     * @param string $id
     * @return Server
     */
    public function setId(string $id): Server
    {
        $this->id = $id;
        return $this;
    }
}
