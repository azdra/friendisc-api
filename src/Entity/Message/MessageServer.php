<?php

namespace App\Entity\Message;

use App\Entity\Server\Channel;
use App\Repository\MessageServerRepository;
use Chivincent\Snowflake\Snowflake;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=MessageServerRepository::class)
 */
class MessageServer
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=255)
     * @Groups({"messageServer", "sendMessage"})
     */
    private string $id;

    /**
     * @ORM\Column(type="text")
     * @Groups({"messageServer", "sendMessage"})
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"messageServer", "sendMessage"})
     */
    private $create_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Server\Channel", inversedBy="messageServers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $channel;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Server\Member", inversedBy="messageServers")
     * @Groups({"messageServer", "sendMessage"})
     */
    private $owner;

    public function __construct()
    {
        $snowflake = new Snowflake();
        $this->id = $snowflake();
        $this->setCreateAt(new \DateTime());
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getChannel(): ?Channel
    {
        return $this->channel;
    }

    public function setChannel(?Channel $channel): self
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * @param mixed $create_at
     */
    public function setCreateAt($create_at): void
    {
        $this->create_at = $create_at;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner): void
    {
        $this->owner = $owner;
    }
}
