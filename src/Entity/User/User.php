<?php

namespace App\Entity\User;

use App\Entity\Server\Member;
use App\Entity\Server\Server;
use App\Repository\UserRepository;
use Chivincent\Snowflake\Snowflake;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class User
 * @package App\Entity\User
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @Vich\Uploadable
 * @UniqueEntity(
 *     fields={"email"},
 *     errorPath="email",
 *     message="There is already an account with this email"
 * )
 * @UniqueEntity(
 *     fields={"id"},
 *     errorPath="id",
 *     message="There is already an account with this id"
 * )
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=255)
     * @Groups({"friends", "login", "sendMessage", "createServer"})
     */
    private string $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(message="Please enter a fist name")
     */
    private ?string $firstname;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(message="Please enter a last name")
     */
    private ?string $lastname;

    /**
     * @ORM\Column(type="string", length=120)
     * @Assert\NotBlank(message="Please enter a username")
     * @Groups({"friends", "login"})
     */
    private ?string $username;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"messageServer", "friends", "login", "sendMessage"})
     *
     * @var string|null
     */
    private $picture;

    /**
     * @Vich\UploadableField(mapping="user_images", fileNameProperty="picture")
     */
    private $picturePath;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"friends", "login"})
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\NotBlank(message="Please enter a mail")
     * @Groups({"login"})
     */
    private ?string $email;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private string $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $salt;

    /**
     * @ORM\Column(type="string", length=12)
     */
    private ?string $local_code;

    /**
     * @ORM\Column(type="json")
     */
    private array $roles = [];

    /**
     * @ORM\Column(type="datetime")
     */
    private ?\DateTimeInterface $create_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTimeInterface $update_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTimeInterface $last_login;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTimeInterface $banned_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTimeInterface $end_ban_at;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Server\Member", mappedBy="user")
     */
    private $members;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Server\Server", mappedBy="owner")
     */
    private $own_servers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User\User")
     * @ORM\JoinTable(name="friend")
     */
    private $friends;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Server\Invite", mappedBy="owner")
     */
    private $invite;

    public function __construct()
    {
        $snowflake = new Snowflake();
        $this->id = $snowflake();
        $this->own_servers = new ArrayCollection();
        $this->members = new ArrayCollection();
        $this->setCreateAt(new \DateTime());
        $this->friends = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function setPicturePath(?File $picturePath = null): void
    {
        $this->picturePath = $picturePath;
        if(null !== $picturePath) {
            $this->update_at = new \DateTime();
        }
    }

    public function getPicturePath(): ?File
    {
        return $this->picturePath;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): void
    {
        $this->picture = $picture;
    }

    public function getSalt(): ?string
    {
        return $this->salt;
    }

    public function setSalt(string $salt): self
    {
        $this->salt = $salt;

        return $this;
    }

    public function getLocalCode(): ?string
    {
        return $this->local_code;
    }

    public function setLocalCode(string $local_code): self
    {
        $this->local_code = $local_code;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->create_at;
    }

    public function setCreateAt(\DateTimeInterface $create_at): self
    {
        $this->create_at = $create_at;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->update_at;
    }

    public function setUpdateAt(?\DateTimeInterface $update_at): self
    {
        $this->update_at = $update_at;

        return $this;
    }

    public function getLastLogin(): ?\DateTimeInterface
    {
        return $this->last_login;
    }

    public function setLastLogin(?\DateTimeInterface $last_login): self
    {
        $this->last_login = $last_login;

        return $this;
    }

    public function getBannedAt(): ?\DateTimeInterface
    {
        return $this->banned_at;
    }

    public function setBannedAt(?\DateTimeInterface $banned_at): self
    {
        $this->banned_at = $banned_at;

        return $this;
    }

    public function getEndBanAt(): ?\DateTimeInterface
    {
        return $this->end_ban_at;
    }

    public function setEndBanAt(?\DateTimeInterface $end_ban_at): self
    {
        $this->end_ban_at = $end_ban_at;

        return $this;
    }

    /**
     * @return Collection|Server[]
     */
    public function getOwnServers(): Collection
    {
        return $this->own_servers;
    }

    public function addOwnServer(Server $ownServer): self
    {
        if (!$this->own_servers->contains($ownServer)) {
            $this->own_servers[] = $ownServer;
            $ownServer->setOwner($this);
        }

        return $this;
    }

    public function removeOwnServer(Server $ownServer): self
    {
        if ($this->own_servers->removeElement($ownServer)) {
            // set the owning side to null (unless already changed)
            if ($ownServer->getOwner() === $this) {
                $ownServer->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Member[]
     */
    public function getMembers(): Collection
    {
        return $this->members;
    }

    public function addMember(Member $member): self
    {
        if (!$this->members->contains($member)) {
            $this->members[] = $member;
            $member->setUser($this);
        }

        return $this;
    }

    public function removeMember(Member $member): self
    {
        if ($this->members->removeElement($member)) {
            // set the owning side to null (unless already changed)
            if ($member->getUser() === $this) {
                $member->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getFriends(): Collection
    {
        return $this->friends;
    }

    public function addFriend(self $friend): self
    {
        if (!$this->friends->contains($friend)) {
            $this->friends[] = $friend;
        }

        return $this;
    }

    public function removeFriend(self $friend): self
    {
        $this->friends->removeElement($friend);

        return $this;
    }


    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     */
    public function setUsername(?string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }
}
