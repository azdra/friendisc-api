<?php


namespace App\Controller\Api\Messages;


use App\Entity\Message\MessageServer;
use App\Entity\Server\Channel;
use App\Entity\Server\Member;
use Doctrine\ORM\EntityManagerInterface;
use http\Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class MessagesController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request $request
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function Create(Request $request, SerializerInterface $serializer): Response
    {
        $data = json_decode($request->getContent(), true);

        $userRepo = $this->entityManager->getRepository(Member::class)->findOneBy([
            'user' => $data['owner']
        ]);

        $channelRepo = $this->entityManager->getRepository(Channel::class)->findOneBy([
            'id' => $data['channel']
        ]);

        $newMessage = new MessageServer();
        $newMessage->setOwner($userRepo);
        $newMessage->setChannel($channelRepo);
        $newMessage->setContent($data['content']);

        $this->entityManager->persist($newMessage);
        $this->entityManager->flush();

        $msgRepo = $this->entityManager->getRepository(MessageServer::class)->findOneBy([
            'id' => $newMessage->getId()
        ]);

        $data = $serializer->serialize($msgRepo, 'json', [
            'groups' => [
                'messageServer'
            ]
        ]);

        return new JsonResponse($data, 200, [], true);
    }

    /**
     * @param Request $request
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function MessagesChannel(Request $request, SerializerInterface $serializer): Response
    {
        $data = json_decode($request->getContent(), true);

        $repo = $this->entityManager->getRepository(MessageServer::class)->findMessageByChannelId($data['channelId'], $data['maxMessages']);

        $data = $serializer->serialize($repo, 'json', [
            'groups' => [
                'messageServer'
            ]
        ]);

        return new JsonResponse($data, 200, [], true);
    }
}
