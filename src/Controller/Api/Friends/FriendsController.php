<?php


namespace App\Controller\Api\Friends;


use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class FriendsController extends AbstractController
{

    private EntityManagerInterface $entityManager;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $id
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function getFriends(string $id, SerializerInterface $serializer): Response
    {
        $repo = $id ? $this->entityManager->getRepository(User::class)->getFriends($id): false;

        $data = $serializer->serialize($repo, 'json', [
            'groups' => [
                'friends'
            ]
        ]);

        return new JsonResponse($data, 200, [], true);
    }

    public function addFriend(Request $request, SerializerInterface $serializer)
    {
        $data = json_decode($request->getContent(), true);

        $friendRepo = $data['friendId'] ? $this->entityManager->getRepository(User::class)->findOneBy(
            ['id' => $data['friendId']]
        ): false;

        $userRepo = $data['userId'] ? $this->entityManager->getRepository(User::class)->findOneBy(
            ['id' => $data['userId']]
        ): false;

        if (!isset($friendRepo) || !isset($userRepo)) {
            return $this->json([
                'error' => 'Impossible to find your friend with this id'
            ], 400);
        }

        $userRepo->addFriend($friendRepo);
        $friendRepo->addFriend($userRepo);
        $this->entityManager->flush();

        $data = $serializer->serialize($friendRepo, 'json', [
            'groups' => [
                'friends'
            ]
        ]);

        return new JsonResponse($data, 200, [], true);
    }
}