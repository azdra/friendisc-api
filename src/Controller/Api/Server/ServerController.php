<?php

namespace App\Controller\Api\Server;

use App\Entity\Server\Channel;
use App\Entity\Server\Member;
use App\Entity\Server\Server;
use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class ServerController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Controller Api Create Server
     * @param Request $request
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function Create(Request $request, SerializerInterface $serializer): Response
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy([
            'id' => $request->request->get('owner')
        ]);

        /** @var UploadedFile $picture */
        $picture = $request->files->get('picture');

        $newServer = new Server();
        $newServer->setOwner($user);
        $newServer->setName($request->request->get('name'));
        if ($picture) $newServer->setPicturePath($picture);
        $this->entityManager->persist($newServer);

        $newChannel = new Channel();
        $newChannel->setType('text');
        $newChannel->setName('General');
        $newChannel->addServer($newServer);
        $this->entityManager->persist($newChannel);

        $newMember = new Member();
        $newMember->setUser($user);
        $newMember->setServer($newServer);
        $newMember->setNickname($user->getUsername());
        $this->entityManager->persist($newMember);

        $this->entityManager->flush();

        $dataServer = $serializer->serialize([
            "server" => $newServer,
            "channel" => $newChannel
        ], 'json', [
            'groups' => [
                'createServer'
            ]
        ]);

        return new JsonResponse($dataServer, 200, [], true);
    }

    public function Delete(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        $serverRepo = $data ? $this->entityManager->getRepository(Server::class)->findOneBy([
            'id' => $data['serverid']
        ]) : false;

        if(!isset($serverRepo)) return $this->json([
            'error' => 'The serverid is empty or this server doesn\'t exist'
        ], 404);

        $this->entityManager->remove($serverRepo);
        $this->entityManager->flush();

        return $this->json([
            'result' => 'The server has been deleted'
        ], 200);
    }

    public function Update(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        $serverRepo = $data ? $this->entityManager->getRepository(Server::class)->findOneBy([
            'id' => $data['serverid']
        ]) : false;

        if(!isset($serverRepo)) return $this->json([
            'error' => 'The serverid is empty or this server doesn\'t exist'
        ], 404);

        if(isset($data['data']['name'])) $serverRepo->setName($data['data']['name']);
        $serverRepo->setUpdateAt(new \DateTime());

        $this->entityManager->persist($serverRepo);
        $this->entityManager->flush();

        return $this->json([
            'result' => 'The server has been updated'
        ], 200);
    }

    /**
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     */
    public function Join(SerializerInterface $serializer, Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        $repo = $this->entityManager->getRepository(Server::class)->findJoinServerByUserId($data['user_id']);

        $data = $serializer->serialize($repo, 'json', [
            'groups' => [
                'loadApp'
            ]
        ]);

        return new JsonResponse($data, 200, [], true);
    }
}
