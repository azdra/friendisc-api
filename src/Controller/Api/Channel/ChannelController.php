<?php


namespace App\Controller\Api\Channel;

use App\Entity\Server\Channel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class ChannelController extends AbstractController
{

    private EntityManagerInterface $entityManager;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param SerializerInterface $serializer
     * @param Request $request
     * @return Response
     */
    public function GetChannel(SerializerInterface $serializer,Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        $repo = $this->entityManager->getRepository(Channel::class)->getChannelWithServerId($data['id']);

        $channel = $serializer->serialize($repo, 'json', [
            'groups' => [
                'serverClick'
            ]
        ]);

        return new JsonResponse($channel, 200, [], true);
    }
}
