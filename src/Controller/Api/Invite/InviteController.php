<?php

namespace App\Controller\Api\Invite;

use App\Entity\Server\Channel;
use App\Entity\Server\Invite;
use App\Entity\Server\Member;
use App\Entity\Server\Server;
use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;

class InviteController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function Create(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        $serverRepo = $this->entityManager->getRepository(Server::class)->findOneBy([
            'id' => $data['server']
        ]) ?? null;

        $userRepo = $this->entityManager->getRepository(User::class)->findOneBy([
            'id' => $data['owner']
        ]) ?? null;

        $newInvite = new Invite();
        $newInvite->setOwner($userRepo);
        $newInvite->setServer($serverRepo);

        $this->entityManager->persist($newInvite);
        $this->entityManager->flush();

        return $this->json([
            'code' => $newInvite->getCode()
        ], 200);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function Delete(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $inviteRepo = $this->entityManager->getRepository(Invite::class)->findOneBy([
            'code' => $data['code'] ?? null
        ]);

        if(!isset($inviteRepo)) return $this->json([
            'error' => 'Invitation code is invalid or does not exist'
        ], 404);

        $this->entityManager->remove($inviteRepo);
        $this->entityManager->flush();

        return $this->json([
            'result' => 'The invite has been deleted'
        ], 200);
    }

    /**
     * @param Request $request
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function JoinServerWithInvite(Request $request, SerializerInterface $serializer): Response
    {
        $data = json_decode($request->getContent(), true);

        $inviteRepo = $this->entityManager->getRepository(Invite::class)->findOneBy([
            'code' => $data['code'] ?? null
        ]);

        $userRepo = $this->entityManager->getRepository(User::class)->findOneBy([
            'id' => $data['user'] ?? null
        ]);

        if(!isset($userRepo)) return $this->json([
            'error' => 'Impossible to recovered the user data'
        ], 404);

        if(!isset($inviteRepo)) return $this->json([
            'error' => 'The invite is invalid or has expired',
            'code' => 'inviteInvalidOrExpired'
        ], 404);

        $memberRepo = $this->entityManager->getRepository(Member::class)->GetIfAlreadyInTheServer($data['user'], $inviteRepo->getServer()->getId());

        if(isset($memberRepo)) return $this->json([
            'error' => 'You are already in the server',
            'code' => 'alreadyInTheServer'
        ], 409);

        $uses = $inviteRepo->getUses()+1;
        $inviteRepo->setUses($uses);

        $newMember = new Member();
        $newMember->setUser($userRepo);
        $newMember->setNickname($userRepo->getUsername());

        $this->entityManager->persist($newMember);
        $inviteRepo->getServer()->addMember($newMember);

        if (((!is_null($inviteRepo->getMaxUses())) && ($inviteRepo->getMaxUses() <= $uses))) {
            $this->entityManager->remove($inviteRepo);
        }
        $this->entityManager->flush();

        $data = $serializer->serialize($inviteRepo, 'json', [
            'groups' => [
                'joinInvite'
            ]
        ]);

        return new JsonResponse($data, 200, [], true);
    }
}