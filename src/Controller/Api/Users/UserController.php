<?php

namespace App\Controller\Api\Users;

use App\Entity\User\User;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\SerializerInterface;

class UserController extends AbstractController
{

    private EntityManagerInterface $entityManager;
    private UserPasswordEncoderInterface $passwordEncoder;
    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * Controller Create User
     * @param Request $request
     * @return Response
     */
    public function Create(Request $request): Response
    {
        $salt = uniqid(uniqid('', false), false);

        $req = $request->request;

        /** @var UploadedFile $picture */
        $picture = $request->files->get('picture');

        $newUser = new User();
        $newUser->setFirstname($req->get('firstname'));
        $newUser->setLastname($req->get('lastname'));
        $newUser->setUsername($req->get('username'));
        $newUser->setEmail($req->get('email'));
        $newUser->setLocalCode('en');
        if ($picture) $newUser->setPicturePath($picture);
        $newUser->setSalt($salt);
        $newUser->setPassword($this->passwordEncoder->encodePassword(
            $newUser,
            $req->get('password').$salt
        ));

        $this->entityManager->persist($newUser);
        $this->entityManager->flush();

        return $this->json([
            "id" => $newUser->getId(),
        ], 201);
    }

    /**
     * Controller Api Login
     * @param Request $request
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function Login(Request $request, SerializerInterface $serializer): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $userRepo = $this->entityManager->getRepository(User::class)->findOneBy(
            ['email' => $data['email']]
        );

        $checkPassword = $userRepo ? $this->passwordEncoder->isPasswordValid($userRepo, $data['password'].$userRepo->getSalt()) : false;

        if (!$userRepo || !$checkPassword) {
            return $this->json([
                "error" => "Bad Password or Bad email"
            ], 401);
        }

        $data = $serializer->serialize($userRepo, 'json', [
            'groups' => [
                'login'
            ]
        ]);

        return new JsonResponse($data, 200, [], true);
    }

    /**
     * Controller Delete User
     * @param Request $request
     * @return JsonResponse
     */
    public function Delete(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $userRepo = $data ? $this->entityManager->getRepository(User::class)->findOneBy(
            ['id' => $data['userid']]
        ) : false;

        if(!isset($userRepo)) return $this->json([
            'error' => 'The userid is empty or this user doesn\'t exist'
        ], 404);

        $this->entityManager->remove($userRepo);
        $this->entityManager->flush();

        return $this->json([
            'result' => 'The user has been deleted'
        ], 200);
    }

    /**
     * Controller Update User
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function Update(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $userRepo = $data ? $this->entityManager->getRepository(User::class)->findOneBy(
            ['id' => $data['userid']]
        ) : false;

        if(!isset($userRepo)) return $this->json([
            'error' => 'The userid is empty or this user doesn\'t exist'
        ], 404);

        if(isset($data['data']['firstname'])) $userRepo->setFirstname($data['data']['firstname']);
        if(isset($data['data']['lastname'])) $userRepo->setLastname($data['data']['lastname']);
        if(isset($data['data']['username'])) $userRepo->setUsername($data['data']['username']);
        if(isset($data['data']['email'])) $userRepo->setEmail($data['data']['email']);
        if(isset($data['data']['local_code'])) $userRepo->setLocalCode($data['data']['local_code']);
/*        if(isset($data['data']['password'])) {
            $userRepo->setPassword($this->passwordEncoder->encodePassword(
                $userRepo,
                $data['data']['password'].$userRepo->getSalt()
            ));
        }*/
        if(isset($data['data']['avatar'])) {
            $userRepo->setPicture($data['data']['avatar']);
        }
        $userRepo->setUpdateAt(new \DateTime());
        $this->entityManager->flush();

        return $this->json([
            'result' => 'The user has been updated'
        ], 200);
    }


    /**
     * @param string $id
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function GetById(string $id, SerializerInterface $serializer): Response
    {
        $userRepo = $id ? $this->entityManager->getRepository(User::class)->findOneBy(
            ['id' => $id]
        ) : false;

        if (!isset($userRepo)) return $this->json([
            'error' => 'The userid is empty or this user doesn\'t exist'
        ], 404);

        $data = $serializer->serialize($userRepo, 'json', [
            'groups' => [
                'login'
            ]
        ]);

        return new JsonResponse($data, 200, [], true);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function ChangePassword(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $userRepo = $data['userid'] ? $this->entityManager->getRepository(User::class)->findOneBy(
            ['id' => $data['userid']]
        ) : false;

        if (!isset($userRepo)) return $this->json([
            'error' => 'The userid is empty or this user doesn\'t exist'
        ], 404);

        $checkPassword = $userRepo ? $this->passwordEncoder->isPasswordValid($userRepo, $data['currentPassword'].$userRepo->getSalt()) : false;

        if (!$checkPassword) {
            return $this->json([
                "error" => "Bad password or the password not given"
            ], 401);
        }

        if(!isset($data['newPassword'])) {
            return $this->json([
                "error" => "The new password has not been given"
            ], 401);
        }

        $userRepo->setPassword($this->passwordEncoder->encodePassword(
            $userRepo,
            $data['newPassword'].$userRepo->getSalt()
        ));

        $userRepo->setUpdateAt(new \DateTime());
        $this->entityManager->flush();

        return $this->json([
            'result' => 'The password has been changed'
        ], 200);
    }
}
