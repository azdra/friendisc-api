<?php


namespace App\Command;

use App\Entity\Message\MessageServer;
use App\Entity\Server\Channel;
use App\Entity\Server\Invite;
use App\Entity\Server\Member;
use App\Entity\Server\Server;
use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker;

class FixturesCommand extends Command
{

    private EntityManagerInterface $entityManager;
    private UserPasswordEncoderInterface $passwordEncoder;

    /**
     * FixturesCommand constructor.
     * @param string|null $name
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(string $name = null, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct($name);

        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    protected static $defaultName = 'app:fixtures';

    protected function configure()
    {
        $this
            ->setDescription('Create 1 user and 50 servers for the dev!')
            ->setHelp('¯\_(ツ)_/¯');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $faker = Faker\Factory::create('fr_FR');
        $salt = uniqid(uniqid('', false), false);

        $newUser = new User();
        $newUser->setFirstName('Pain');
        $newUser->setLastName('D\'épice');
        $newUser->setEmail('pain.depice@mail.com');
        $newUser->setUsername('Pain D\'épice');
        $newUser->setLocalCode('en');
        $newUser->setSalt($salt);
        $newUser->setStatus('You will not pass');

        $newUser->setPassword($this->passwordEncoder->encodePassword(
            $newUser,
            'azerty'.$salt
        ));

        $this->entityManager->persist($newUser);
        $output->writeln('User 1 Create');

        $newUser2 = new User();
        $newUser2->setFirstName('John');
        $newUser2->setLastName('Titor');
        $newUser2->setEmail('john.titor@mail.com');
        $newUser2->setUsername('John Titor');
        $newUser2->setLocalCode('en');
        $newUser2->addFriend($newUser);
        $newUser->addFriend($newUser2);
        $newUser2->setSalt($salt);

        $newUser2->setPassword($this->passwordEncoder->encodePassword(
            $newUser,
            'azerty'.$salt
        ));

        $this->entityManager->persist($newUser2);
        $output->writeln('User 2 Create');

        for ($u = 0; $u <= 5; $u++) {
            $newUuser = new User();
            $newUuser->setFirstName($faker->firstName);
            $newUuser->setLastName($faker->lastName);
            $newUuser->setEmail($faker->email);
            $newUuser->setUsername($faker->userName);
            $newUuser->setLocalCode('en');
            $newUuser->addFriend($newUser);
            $newUser->addFriend($newUuser);
            $newUuser->setSalt($salt);
            $newUuser->setPassword($this->passwordEncoder->encodePassword(
                $newUuser,
                    'azerty'.$salt
            ));
            $this->entityManager->persist($newUuser);
        }

        for ($i = 0; $i <= 5; $i++) {
            $newServer = new Server();
            $newServer->setName('Server_'.$i);
            $newServer->setOwner($newUser);
            $this->entityManager->persist($newServer);

            $newMember = new Member();
            $newMember->setUser($newUser);
            $newMember->setServer($newServer);
            $newMember->setNickname($newUser->getUsername());
            $this->entityManager->persist($newMember);

            if ($i === 1) {
                $newMember = new Member();
                $newMember->setUser($newUser2);
                $newMember->setServer($newServer);
                $newMember->setNickname($newUser2->getUsername());
                $this->entityManager->persist($newMember);
            }

            for ($c = 0; $c <= 10; $c++) {
                $newChannel = new Channel();
                $newChannel->setType('text');
                $newChannel->setName($faker->name);
                $newChannel->setTopic($faker->realText(rand(10, 255), 2));
                $newChannel->addServer($newServer);
                $this->entityManager->persist($newChannel);

                for($k = 0; $k <= 60; $k++){
                    $newMessage = new MessageServer();
                    $newMessage->setContent($faker->text);
                    $newMessage->setChannel($newChannel);
                    $newMessage->setOwner($newMember);
                    $this->entityManager->persist($newMessage);
                }
            }
        }
        $output->writeln('Servers Creates');
        $output->writeln('Channels Create');
        $output->writeln('Messages Creates');
        $output->writeln('Members Creates');

        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
