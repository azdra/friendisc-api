<?php


namespace App\Command;

use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker;

class AddFriends extends Command
{

    private EntityManagerInterface $entityManager;
    private UserPasswordEncoderInterface $passwordEncoder;

    /**
     * FixturesCommand constructor.
     * @param string|null $name
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(string $name = null, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct($name);

        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    protected static $defaultName = 'app:friends';

    protected function configure()
    {
        $this
            ->setDescription('friend')
            ->setHelp('friend');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $salt = uniqid(uniqid('', false), false);

        $newUser = new User();
        $newUser->setFirstName('Pain');
        $newUser->setLastName('D\'épice');
        $newUser->setEmail('pain.depice@mail.com');
        $newUser->setUsername('Pain D\'épice');
        $newUser->setLocalCode('en');
        $newUser->setSalt($salt);
        $newUser->setStatus('You will not pass');

        $newUser->setPassword($this->passwordEncoder->encodePassword(
            $newUser,
            'azerty'.$salt
        ));

        $this->entityManager->persist($newUser);
        $output->writeln('User 1 Create');

        $newUser2 = new User();
        $newUser2->setFirstName('John');
        $newUser2->setLastName('Titor');
        $newUser2->setEmail('john.titor@mail.com');
        $newUser2->setUsername('John Titor');
        $newUser2->setLocalCode('en');
        $newUser2->setSalt($salt);
        $newUser2->addFriend($newUser);
        $newUser->addFriend($newUser2);

        $newUser2->setPassword($this->passwordEncoder->encodePassword(
            $newUser,
            'azerty'.$salt
        ));

        $this->entityManager->persist($newUser2);
        $output->writeln('User 2 Create');

        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
