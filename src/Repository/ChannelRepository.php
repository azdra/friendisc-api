<?php


namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class ChannelRepository
 * @package App\Repository
 */
class ChannelRepository extends EntityRepository
{
    public function getChannelWithServerId($serverId)
    {
        return $this->createQueryBuilder('c')
            ->leftJoin('c.servers', 's')
//            ->addSelect('s')
            ->andWhere('s.id = :serverId')
            ->setParameter('serverId', $serverId)
            ->getQuery()
            ->getResult();
    }
}
