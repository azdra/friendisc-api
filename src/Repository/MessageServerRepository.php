<?php

namespace App\Repository;

use App\Entity\Message\MessageServer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MessageServer|null find($id, $lockMode = null, $lockVersion = null)
 * @method MessageServer|null findOneBy(array $criteria, array $orderBy = null)
 * @method MessageServer[]    findAll()
 * @method MessageServer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageServerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MessageServer::class);
    }

    public function findMessageByChannelId($channelId, $maxResults = 50)
    {
        return $this->createQueryBuilder('m')
            ->leftJoin('m.channel', 'c')
            ->leftJoin('m.owner', 'owner')
            ->leftJoin('owner.user', 'user')
            ->andWhere('c.id = :channelId')
            ->setParameter('channelId', $channelId)
            ->setMaxResults($maxResults)
            ->orderBy('m.create_at', 'DESC')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return MessageServer[] Returns an array of MessageServer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MessageServer
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
