<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class MemberRepository extends EntityRepository
{
    public function GetIfAlreadyInTheServer($userId, $serverId)
    {
        return $this->createQueryBuilder('m')
            ->leftJoin('m.server', 's')
            ->leftJoin('m.user', 'u')
            ->andWhere('s.id = :serverId')
            ->andWhere('u.id = :userId')
            ->setParameter('userId', $userId)
            ->setParameter('serverId', $serverId)
//            ->orderBy('u.username', 'ASC')
            ->getQuery()
            ->getOneOrNullResult();
    }
}
