<?php

namespace App\Repository;
use Doctrine\ORM\EntityRepository;

/**
 * Class ServerRepository
 * @package App\Repository
 */
class ServerRepository extends EntityRepository
{
    public function findJoinServerByUserId($userId)
    {
        return $this->createQueryBuilder('s')
            ->leftJoin('s.members', 'm')
            ->leftJoin('m.user', 'u')
            ->leftJoin('s.channels', 'c')
            ->andWhere('u.id = :userid')
            ->setParameter('userid', $userId)
            ->getQuery()
            ->getResult();
    }
}
