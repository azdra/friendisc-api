<?php

namespace App\Repository;
use Doctrine\ORM\EntityRepository;

/**
 * Class UserRepository
 * @package App\Repository
 */
class UserRepository extends EntityRepository
{
    public function getFriends($userId)
    {
        return $this->createQueryBuilder('u')
            ->leftJoin('u.friends', 'f')
            ->andWhere('f.id = :userId')
            ->setParameter('userId', $userId)
            ->orderBy('u.username', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
